import {
  Injectable,
  EventEmitter
} from "@angular/core";
import {
  Http,
  Response
} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {
  Observable
} from 'rxjs/Rx';
import {
  Router
} from "@angular/router";
import { AppComponent } from "./app.component";


@Injectable()
export class TrelloService {
  localStorage: any;
  mostrarMenuEmitter = new EventEmitter < boolean > ();
  resToken: any;

  private arrayCards: any[];
  private baseUrl: string;
  private key: string;
  private token: string;
  private idList: string;
  private idBoard:string;
  private idMember:any;

  constructor(private http: Http, private router: Router) {
    this.baseUrl = "https://api.trello.com/1/";
    this.key = "&key=136ea693d8ffd1de92fb99f769578c7b";
    this.idList = "&idList=59e69f56ba326fc43afb52fe";
    this.token = "&token=" + localStorage.getItem('token');
    this.idMember = localStorage.getItem('member');
    this.idBoard = localStorage.getItem('idBoard');
    AppComponent.projectSet.subscribe(
      idBoard => {this.idBoard = idBoard; localStorage.setItem('idBoard', idBoard);}
    );
    // this.mostrarMenuEmitter.subscribe(
    //   idBoard => {
    //     this.idBoard = idBoard; console.log(idBoard);
    //   } 
    // );
  }

  ngOnInit() {
    this.getToken(localStorage.getItem('token'));
  }

  getMemberByid(idMember){
    return this.http.get(this.baseUrl + "members/" + idMember + "?" + this.key + this.token)
      .map(res => res.json());
  }

  getMemberFromBoard(){
    return this.http.get(this.baseUrl + "boards/" + this.idBoard + "/members?" + this.key + this.token)
      .map(res => res.json());
  }

  getMemberFromCard(cardId){
    return this.http.get(this.baseUrl + "cards/" + cardId + "/members?" + this.key + this.token)
      .map(res => res.json());
  }

  getSelf(){
    return this.http.get(this.baseUrl + "members/" + this.idMember + "?" + this.key + this.token)
      .map(res => res.json());
  }

  getMyBoards(){
    return this.http.get(this.baseUrl + "members/" + this.idMember + "/boards?" + this.key + this.token)
      .map(res => res.json());
  }

  getAllCards() {

    return this.http.get(this.baseUrl + "boards/" + this.idBoard + "/cards?" + this.key + this.token)
      .map(res => res.json());
    //console.log(arrayDados);
  }

  getCardId(id: number) {
    return this.http.get(this.baseUrl + "cards/" + id + "?" + this.key + this.token)
      .map(res => res.json());
    //console.log(arrayDados);
  }

  getCardsModule(idList: string) {
    return this.http.get(this.baseUrl + "lists/" + idList + "/cards?" + this.key + this.token)
      .map(res => res.json());
    //console.log(arrayDados);
  }

  getList(idList: string) {
    return this.http.get(this.baseUrl + "lists/" + idList + "?" + this.key + this.token)
      .map(res => res.json());
    //console.log(arrayDados);
  }

  getGameObjects() {
    return this.http.get(this.baseUrl + "boards/" + this.idBoard + "/lists?cards=open&card_fields=id,labels,idBoard&filter=open&fields=name" + this.key + this.token)
      .map(res => res.json());
    //console.log(arrayDados);
  }

  getNameBoard() {
    return this.http.get(this.baseUrl + "boards/" + this.idBoard + "?fields=id,name" + this.key + this.token)
      .map(res => res.json());
    //console.log(arrayDados);
  }

  getLabelsBoard() {
    return this.http.get(this.baseUrl + "boards/" + this.idBoard + "?labels=all&label_fields=all&fields=id" + this.key + this.token)
      .map(res => res.json());
    //console.log(arrayDados);
  }
  getNameGO(idGO: string) {
    return this.http.get(this.baseUrl + "lists/" + idGO + "?fields=name" + this.key + this.token)
      .map(res => res.json());
    //console.log(arrayDados);
  }
  postCards(card) {
    return this.http.post(this.baseUrl + "cards?" +card +this.key+ this.token, card)
      .map(res => res.json());
  }

  postGameObject(params){
    return this.http.post(this.baseUrl + "lists?" + params +"&idBoard=" + this.idBoard +  this.key + this.token, params)
      .map(res => res.json());
  }

  setLabels(idBoard, label, color){
    this.http.post(this.baseUrl + "boards/" + idBoard + "/labels?name="+ label+"&color="+ color +  this.key + this.token, idBoard)
      .map(res => res.json()).subscribe(
        data => {
          console.log(data);
        },
        error => console.log(error)      
      );
  }

  // setLabelsCard(idCard, label, color){
  //   this.http.post(this.baseUrl + "cards/" + idCard + "/labels?name="+ label+"&color="+ color +  this.key + this.token, idBoard)
  //     .map(res => res.json()).subscribe(
  //       data => {
  //         console.log(data);
  //       },
  //       error => console.log(error)      
  //     );
  // }


  postProject(params){
    return this.http.post(this.baseUrl + "boards/?" + params +  this.key + this.token, params)
      .map(res => res.json());
  }

  editCards(idCard, params) {
    return this.http.put(this.baseUrl + "cards/"+ idCard +"?"+params + this.key + this.token, params)
    .map(res => res.json());
  }

  addMemberToProject(idMember) {
    return this.http.put(this.baseUrl + "boards/"+ this.idBoard +"/members/"+ idMember+ "?type=normal" + this.key + this.token, idMember)
    .map(res => res.json());
  }

  deleteCard(id) {
    return this.http.delete(this.baseUrl + "cards/" + id + "?" + this.key + this.token)
      .map(res => res.json());
  }

  deleteGO(id){
    return this.http.put(this.baseUrl + "lists/" + id + "/closed?value=true" + this.key + this.token, id)
      .map(res => res.json());
  }

  getToken(token: string) {
    return this.http.get(this.baseUrl + "tokens/" + token + "?token=" + token + this.key)
      .map(res => res.json()).subscribe(
        data => {
          this.resToken = data;
          if (this.resToken.length == 0) {
            this.mostrarMenuEmitter.emit(false);
            this.router.navigate(['']);
          } else {
            localStorage.setItem('token', token);
            localStorage.setItem('member', data.idMember);
            this.idMember = localStorage.getItem('member');
            this.token = "&token=" + localStorage.getItem('token');
            this.mostrarMenuEmitter.emit(true);
            this.router.navigate(['/cards']);
            location.reload();
          }
        },
        error => console.log(error),
        () => console.log(this.resToken)
      );
  }
}
