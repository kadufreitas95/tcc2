import { TrelloService } from './../trello.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  memberId:string;
  memberName:string;
  constructor(private trelloService: TrelloService) { }

  ngOnInit() {
    this.trelloService.getSelf().subscribe(
      response =>{
        this.memberName = response.fullName;
        this.memberId = response.id;
      }
    );
  }

}
