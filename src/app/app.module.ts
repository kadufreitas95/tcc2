import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

//import { routing } from './app.routing';
import { CardsModule } from './cards/cards.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { CardDetalheComponent } from './card-detalhe/card-detalhe.component';
import { TrelloService } from './trello.service';
//import { RouterModule } from '@angular/router';
//import { GameObjectsComponent } from './game-objects/game-objects.component';
//import { SubModuleArteComponent } from './sub-module-arte/sub-module-arte.component';
import { ArteModule } from './arte/arte.module';
import { AppRoutingModule } from './app.routing.module';
import { GameObjectsModule } from './game-objects/game-objects.module';
import { LoginComponent } from './login/login.component';
import { FormsModule} from '@angular/forms';

import { ManagementComponent } from './management/management.component';
import { AddMembroComponent } from './add-membro/add-membro.component';
import { NavVerticalComponent } from './nav-vertical/nav-vertical.component';
import { TimeLineComponent } from './time-line/time-line.component';
import { PerfilComponent } from './perfil/perfil.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ManagementComponent,
    AddMembroComponent,
    TimeLineComponent,
    PerfilComponent
    //CardDetalheComponent,
    //GameObjectsComponent,
    //SubModuleArteComponent
  ],
  imports: [
    BrowserModule,
    ArteModule,
    CardsModule,
    GameObjectsModule,
    FormsModule,
   // RouterModule
    AppRoutingModule
  ],
  exports:[],
  providers: [TrelloService],
  bootstrap: [AppComponent]
})
export class AppModule { }
