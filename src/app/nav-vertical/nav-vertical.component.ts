import { CardsService } from './../cards/cards.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nav-vertical',
  templateUrl: './nav-vertical.component.html',
  styleUrls: ['./nav-vertical.component.css']
})
export class NavVerticalComponent implements OnInit {

  @Input() styleNav:number;
  @Input() nameGO:string;
  @Input() idList:any;
  @Output() delete = new EventEmitter();

  showGO:boolean = false;
  showManagement:boolean = false;

  constructor() { }

  ngOnInit() {
    if (this.styleNav == 0) {// Exibi o nav para os Game Objects
      this.showGO = true;
    }else if(this.styleNav == 1) {// Exibi o nav para os Management
      this.showManagement = true;
    }
  }

  deleteGO(){
    var res;
    var r = confirm("Você tem certeza que deseja deletar esse Game Object?");
    if (r == true) {
      this.delete.emit(true); 
    }
  }

}
