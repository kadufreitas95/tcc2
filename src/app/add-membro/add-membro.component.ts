import { TrelloService } from './../trello.service';
import { Component, OnInit } from '@angular/core';
declare var $:any;

@Component({
  selector: 'add-membro',
  templateUrl: './add-membro.component.html',
  styleUrls: ['./add-membro.component.css', '/.././app.component.css']
})
export class AddMembroComponent implements OnInit {

  idMember:string;
  memberName:string;
  submitId:string;
  alert:boolean = false;
  listMembers:any;
  dadosMember:boolean = false;

  constructor(private trelloService: TrelloService) { }

  ngOnInit() {
    this.trelloService.getMemberFromBoard()
    .subscribe(
      data =>{this.listMembers = data}
    );
  }

  verificaId(){
    this.trelloService.getMemberByid(this.idMember).subscribe(
      response =>{
        this.submitId = response.id;
        this.memberName = response.fullName;
        this.dadosMember = true;
      },
      error=>{
        if (error != null) {
          alert("Não foi possivel encontrar este membro, verifique o identificador e tente novamente.");
        }
      }
    );
  }

  addMember(){
    this.trelloService.addMemberToProject(this.submitId)
    .subscribe(
      data => {
        console.log(data);
        // $( "#dadosMember" ).hide();
        this.dadosMember = false;
        this.alert = true;
      },
      error => {
        console.log(error);
        if (error != null) {
          alert("Error. Tivemos um problema ao adicionar este membro, tente novamente mais tarde.");
        }
      }
    );
  }

}
