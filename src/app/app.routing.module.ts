import { NgModule } from "@angular/core";
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ArteComponent } from './arte/arte.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from "./login/login.component";
import { ManagementComponent } from "./management/management.component";
import { PerfilComponent } from "./perfil/perfil.component";
import { TimeLineComponent } from "./time-line/time-line.component";

const appRoutes : Routes = [
    //{path: "cards", component: CardsComponent },
    //{path: "cards/:id", component: CardDetalheComponent },
    // {path: "arte", component: ArteComponent },
    {path: "perfil", component: PerfilComponent },
    {path: "login", component: LoginComponent },
    {path: "timeLine", component: TimeLineComponent },
    {path: "management", component: ManagementComponent },
    {path: "", component: HomeComponent }
];

@NgModule({
    imports:[RouterModule.forRoot(appRoutes)],
    exports:[RouterModule]
})
export class AppRoutingModule{

}