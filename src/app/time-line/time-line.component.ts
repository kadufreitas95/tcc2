import { CardsService } from './../cards/cards.service';
import { TrelloService } from './../trello.service';
import { Router } from '@angular/router';
import { Component, OnInit, ElementRef } from '@angular/core';
import * as moment from 'moment';

declare var vis:any;
declare var $:any;

@Component({
  selector: 'time-line',
  templateUrl: './time-line.component.html',
  styleUrls: ['./time-line.component.css']
})
export class TimeLineComponent implements OnInit {

  refTimeline:any; 
  count:number = 1;
  items:any = []; 
  items2:any = new Array();
  dataItems:any; 
  arrayLabels:any = {};
  tasksList:any;
  type:number = 3;
  state:number = 4;
  arrayMyCards:any = [];
  memberId:any;
  myCardsFlag:boolean = false;
  selected:boolean = true;
  refAnteriorState:any = '';
  refAnteriorType:any = '';
  refAnteriorMyCard:any = '';

  constructor(private element: ElementRef, private trelloService:TrelloService, private router: Router, private cardsService:CardsService) {
    this.memberId = localStorage.getItem('member');
   }

  ngOnInit() {   
    this.trelloService.getLabelsBoard().subscribe(
      data => {
        data.labels.forEach(element => {
          this.arrayLabels[element.name] = element.id;
        });
        this.trelloService.getAllCards()
        .subscribe(
          cards => {
            this.drawTarefas(cards, false);
            this.tasksList = cards;
            cards.forEach(card => {
              card.idMembers.forEach(id => {
                if (id == this.memberId) {
                  this.arrayMyCards.push(card);
                }
              });
            });   
          },
          error => console.log(error)
        );
    });
   
  }

  drawTarefas(cards, filter){
    if (cards.length == 0) {
      var myArray = [];
      this.updateTimeLine(myArray); 
      return;
    }
    let data = this.getDate(cards[0]['due']);
    var myArray = [];
    cards.forEach(card => {
      let data = this.getDate(card.due);
      var state:number;
      card.labels.forEach(label => {
        if(label.id == this.arrayLabels.fazer){ state = 1;}
        if(label.id == this.arrayLabels.desenvolvendo){ state = 2;} 
        if(label.id == this.arrayLabels.feito){ state = 3;} 
      }); 
      if(state == null){
        
      }

      if (this.getNullDate(card.due)){
        let dataPoint = this.getDate(new Date());
        if (state == 3){
          var cardContent = '<div  class="card myCard text-white bg-success">\
                        <div class="card-body p-2">\
                          <h5 class="card-title">'+ card.name+'</h5>\
                        </div>\
                      </div>';   
          myArray.push({id: card.id , content: cardContent, start: dataPoint, type: 'point'});
        }else{
          var cardContent = '<div  class="card myCard text-black">\
                        <div class="card-body p-2">\
                          <h5 class="card-title">'+ card.name+'</h5>\
                        </div>\
                      </div>';                  
          myArray.push({id: card.id , content: cardContent, start: dataPoint, type: 'point'});
        }
      }else{
        if (this.getdelayedDate(card.due, state)){
          var cardContent = '<div  class="card myCard text-white bg-danger">\
                        <div class="card-body p-2">\
                          <h5 class="card-title">'+ card.name+'</h5>\
                        </div>\
                      </div>';      
          myArray.push({id: card.id , content: cardContent, start: data});
        }else if (state == 3){
          var cardContent = '<div  class="card myCard text-white bg-success">\
                        <div class="card-body p-2">\
                          <h5 class="card-title">'+ card.name+'</h5>\
                        </div>\
                      </div>';   
          myArray.push({id: card.id , content: cardContent, start: data});
        }else{
          var cardContent = '<div  class="card myCard text-black">\
                        <div class="card-body p-2">\
                          <h5 class="card-title">'+ card.name+'</h5>\
                        </div>\
                      </div>';                  
          myArray.push({id: card.id , content: cardContent, start: data});
        }
      }
    });
    if (filter) {
      this.updateTimeLine(myArray);  
    }else{
      this.setTimeLine(myArray);
    }
  }

  getNullDate(date){
    var d = new Date(date);
    var currentDate = new Date();

    if (d.getFullYear() < (currentDate.getFullYear() - 3)) {
      return true
    }else{
      return false;
    }
  }

  getdelayedDate(date, state){
    var d = new Date(date);
    var currentDate = new Date();
    if (state < 3) {
      if (d < currentDate) {
        return true
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  getDate(date){
    var d = new Date(date);
    
    
    if ((d.getDate() + 1) < 10) {
      var day = '0' + d.getDate(); // Hours
    }else{
      var day = '' + d.getDate(); // Hours
    }
    //asdasd
    if ((d.getMonth() + 1) < 10) {
      var month = '0' + (d.getMonth() + 1);  
    }else{
      var month = '' + (d.getMonth() + 1);
    }
    var year = '' + d.getFullYear();

    return  year+'-'+ month+'-' + day ;
  }

  updateTimeLine(items){
    this.refTimeline.setItems(items);
    this.refTimeline.redraw();
  }

  redirect(properties){
      location.href = '/cards/' + properties.items[0];
  }

  setTimeLine(myArray){
   var container = document.getElementById('visualization');
  
   var options = {
    width: '100%',
    minHeight: '300px',
    maxHeight: '500px',
    margin: {
      item: 20
    },
    moment: function(date) {
      return vis.moment(date).utcOffset('+00:00');
    }
   };
  //  var timeline = new vis.Timeline(container, items, options);
  var timeline = new vis.Timeline(this.element.nativeElement, myArray, options);
  var now = new Date();
  var currentDate = new Date(now.getTime() - (3*1000*60*60));
  timeline.setCurrentTime(new Date(currentDate));

  timeline.on('select', this.redirect);

   /**
    * Move the timeline a given percentage to left or right
    * @param {Number} percentage   For example 0.1 (left) or -0.1 (right)
    */
   function move (percentage) {
       var range = timeline.getWindow();
       var interval = range.end - range.start;

       timeline.setWindow({
           start: range.start.valueOf() - interval * percentage,
           end:   range.end.valueOf()   - interval * percentage
       });
   }
   // attach events to the navigation buttons
   document.getElementById('zoomIn').onclick    = function () { timeline.zoomIn( 0.2); };
   document.getElementById('zoomOut').onclick   = function () { timeline.zoomOut( 0.2); };
   document.getElementById('moveLeft').onclick  = function () { move( 0.2); };
   document.getElementById('moveRight').onclick = function () { move(-0.2); };
  // var timeline = new vis.Timeline(this.element.nativeElement, items, options);
  this.refTimeline = timeline;
  }

  focusToDay(){
    let now = new Date();
    now.setDate(now.getDate() - 7);
    let next = new Date();
    next.setDate(next.getDate() + 7);
    var formatNow = this.cardsService.setDate(now);
    var formatnext = this.cardsService.setDate(next);
    this.refTimeline.setWindow(formatNow, formatnext);
  }

  toggleSelect(event, ref){
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = "#" + idAttr.nodeValue;
    
    if (ref != value) {
      $(ref).removeClass( "selected" );
      $(value).addClass( "selected" );
      return value;
    }
    return ref;
  }

  filterType(type, event){
    this.type = type;
    this.filtros(this.type,this.state);
    this.refAnteriorType = this.toggleSelect(event, this.refAnteriorType);
  }
  

  filterState(state, event){
    this.state = state;
    this.filtros(this.type,this.state);
    this.refAnteriorState = this.toggleSelect(event, this.refAnteriorState);    
  }

  myCardsOn(flag, event){
    if (flag == 1) {
      this.myCardsFlag = true;
      this.drawTarefas(this.arrayMyCards,true);  
    }else{
      this.myCardsFlag = false;
      this.drawTarefas(this.tasksList,true);  
    }
    $(this.refAnteriorState).removeClass( "selected" );
    $(this.refAnteriorType).removeClass( "selected" );
    this.type = 3;
    this.state = 4;

    this.refAnteriorMyCard = this.toggleSelect(event, this.refAnteriorMyCard);
    
  }



  filtros(type,state){

    if (this.myCardsFlag) {
      var arrayForEach = this.arrayMyCards;
    }else{
      var arrayForEach = this.tasksList;
    }
    
    var cardState:number;
    var cardType:number;
    var arrayCards:any = [];
    arrayForEach.forEach(card => {
      card.labels.forEach(label => {
        if(label.id == this.arrayLabels.arte){ cardType = 1; }
        if(label.id == this.arrayLabels.programacao){ cardType = 2;} 
        if(label.id == this.arrayLabels.fazer){ cardState = 1;}
        if(label.id == this.arrayLabels.desenvolvendo){ cardState = 2;} 
        if(label.id == this.arrayLabels.feito){ cardState = 3;} 
      }); 
      
      if (cardType == type && cardState == state) { 
        arrayCards.push(card);  
      }else if (cardType == type && state == 4) {
        arrayCards.push(card);  
      }else if (type == 3 && cardState == state) {
        arrayCards.push(card);  
      }else if (type == 3 && state == 4) {
        arrayCards.push(card);  
      }
      

    });
    
    this.drawTarefas(arrayCards,true);  
  }


}
