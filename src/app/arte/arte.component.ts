import { Component, OnInit } from '@angular/core';
import { ArteService } from './arte.service';
import { TrelloService } from '../trello.service';

@Component({
  selector: 'app-arte',
  templateUrl: './arte.component.html',
  styleUrls: ['./arte.component.css']
})
export class ArteComponent implements OnInit {

  nomeModuloArte:string;
  elementosModuloArte:string[];

  constructor(private trelloService:TrelloService){}

  ngOnInit(){

  }

}
