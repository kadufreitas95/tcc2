import { TestBed, inject } from '@angular/core/testing';

import { ArteService } from './arte.service';

describe('ArteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArteService]
    });
  });

  it('should be created', inject([ArteService], (service: ArteService) => {
    expect(service).toBeTruthy();
  }));
});
