import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArteComponent } from './arte.component';
import { ArteService } from './arte.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ArteComponent],
  exports:[
    ArteComponent
  ],
  providers:[
    ArteService
  ]
})
export class ArteModule { }
