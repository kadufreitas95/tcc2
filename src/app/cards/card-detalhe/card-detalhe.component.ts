import { CardsService } from './../cards.service';
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TrelloService } from '../../trello.service';

@Component({
  selector: 'card-detalhe',
  templateUrl: './card-detalhe.component.html',
  styleUrls: ['./card-detalhe.component.css','../.././app.component.css','.././cards.component.css']
})
export class CardDetalheComponent implements OnInit {

  idList:number;
  inscricao: Subscription;
  card:any;
  error:any;
  nameGO:string;
  date_entrega:any;
  //@Input() cardId:any;
  @Output() response = new EventEmitter();
  arrayLabels:any = {};
  listMembersOfCard:any;
  cardId:any;
  direct:boolean = false;
  
  constructor(
    private route: ActivatedRoute,
    private trelloService: TrelloService,
    private router: Router,
    private cardsService: CardsService
  ) {

    this.inscricao = this.route.params.subscribe(
      (params:any) => {
        if (params['id'] != null) {
          this.cardId = params['id'];  
          this.getCard(this.cardId);
          this.direct = true;
        }else{
          this.cardsService.cardEmitter.subscribe(
            idCard => {
              this.cardId = idCard;
              this.getCard(this.cardId);
            }
          );
        }
      }
    );
   }

  ngOnInit() {
    this.trelloService.getLabelsBoard().subscribe(
      data => {
        data.labels.forEach(element => {
          this.arrayLabels[element.name] = {id:element.id, color:element.color};
        });
      }
    );
  }

  setColor(idlabel){
    let color:any;
    
    if(idlabel == this.arrayLabels.arte.id){color = '#f2d600'; }
    if(idlabel == this.arrayLabels.programacao.id){ color = '#c377e0';} 
    if(idlabel == this.arrayLabels.fazer.id){ color = '#272727';}
    if(idlabel == this.arrayLabels.desenvolvendo.id){ color = '#d29034';} 
    if(idlabel == this.arrayLabels.feito.id){ color = '#61bd4f';  } 
    
    return color;
  }

  delete(id:string){
    var res;
    var r = confirm("Você tem certeza que deseja deletar esse card?");
    if (r == true) {
      this.trelloService.deleteCard(id)
      .subscribe(
        data => res = data,
        error => this.error = error,
        () => {
          if(this.error == null){
            // this.router.navigate(['game-objects/sub-module-arte/', this.idList, this.nameGO]);
            this.response.emit(true);
          }
        }
      ); 
    }
  }

  getCard(idCard){
    this.trelloService.getCardId(idCard)
    .subscribe(
      data => {
        this.card = data;
        this.idList = this.card.idList;
        this.trelloService.getNameGO(data.idList).subscribe(
          go => this.nameGO = go.name
        );
        this.date_entrega = this.cardsService.getDate(data.due);
        
      },
      error => console.log(error),
      () => console.log()
    );
    this.trelloService.getMemberFromCard(idCard)
      .subscribe(
        data =>{
          this.listMembersOfCard = data;
          // this.card.id = data
        }
      );
  }

}
