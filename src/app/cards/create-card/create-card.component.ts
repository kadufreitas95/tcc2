import { CardsService } from './../cards.service';
import { Component, OnInit } from '@angular/core';
import { TrelloService } from '../../trello.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-create-card',
  templateUrl: './create-card.component.html',
  styleUrls: ['./create-card.component.css','../.././app.component.css']
})
export class CreateCardComponent implements OnInit {

  private idList:string;
  private nameGO:string;
  private type:number;
  private response:any;
  private card:any;
  
  id:number;
  inscricao: Subscription;
  error:any;
  notification:any = {
    type: '',
    text: ''
  };
  backGroundCard:string = "#fff";
  display:string = 'none';
  formulario: FormGroup;
  arrayLabels:any = {};
  listMembers:any;
  listSelectedMembers:any = new Array();
  flag:any = new Array;


  constructor(private route: ActivatedRoute, private trelloService:TrelloService, private router: Router, private cardsService: CardsService) { }

  ngOnInit() {
    this.inscricao = this.route.params.subscribe(
      (params:any) => {
          this.idList = params['idList'];
          this.nameGO = params['nameGO'];
          this.type = params['type'];      
      }
    );
    this.trelloService.getLabelsBoard().subscribe(
      data => {
        data.labels.forEach(element => {
          // console.log(element);
          this.arrayLabels[element.name] = element.id;
          // this.arrayLabels.push(element.id);
        });
      }
    );

    this.trelloService.getMemberFromBoard()
    .subscribe(
      data =>{
        console.log(data);
        this.listMembers = data
      }
    );
  }

  onSubmit(form){
    var params:string;
    params = "&name=" + form.value['nome'];
    params += "&desc=" + form.value['desc'];
    if (form.value['bday'] != '') {
      // params += "&due=" + form.value['bday'];  
      params += "&due=" + this.cardsService.setDateSubmit(form.value['bday']);  
    }
    params += "&idList=" + this.idList;
    params += "&idLabels=";
    //params += "&idList=";    
    switch (form.value['status']) {
      case '1'://Fazer
        params += this.arrayLabels.fazer;  
        break;
      case '2'://Desenvolvendo
        params += this.arrayLabels.desenvolvendo;
        break;        
      case '3'://Feito
        params += this.arrayLabels.feito;
        break;
    
      default:
        break;
    }
    if (form.value['type'] == 0) {//type = Arte
      params += ","+this.arrayLabels.arte;
    }else{//type = Programacao
      params += ","+this.arrayLabels.programacao;
    }

    for (var i = 0; i < this.listSelectedMembers.length; i++) {
      if (i > 0) {
        params += ","+ this.listSelectedMembers[i]['id'];
      }else{
        params += "&idMembers="+ this.listSelectedMembers[i]['id'];        
      }
      
    }
    console.log(params);

    this.trelloService.postCards(params)
    .subscribe(
      data => {
        this.response = data;
        form.resetForm();
      },
      error => this.error = error
    );
    if (this.error != null) {
      this.notification.type = "Erro!";
      this.notification.text = "Tivemos um problema com a criação do seu card, tente novamente mais tarde.";
    }else{
      this.notification.type = "Sucesso!";
      this.notification.text = "Seu card foi criado.";
      this.display = 'block';
    }
  }
  
  toggleColor(type){
    if (type == 0) {
      this.backGroundCard = "#FBBA42";
    }else{
      this.backGroundCard = "#56B9D0";
    }    
  }

  back(){
    // game-objects/sub-module-arte/:idList/:nameGO
    if(this.type == 0){
      var subModule = "sub-module-arte";
    }else if(this.type == 1){
      var subModule = "sub-module-programacao";
    }
    this.router.navigate(['/game-objects/' + subModule, this.idList]);
  }

  addSelectMember(member){  
    let selectedMember:any = {
      nome: member.fullName,
      id:member.id
    };
    let verifica = 0;

    this.flag.forEach(element => {
      if (element == selectedMember.id) {
        verifica ++;
      }      
    });

    if (verifica == 0) {
      this.listSelectedMembers.push(selectedMember);  
      this.flag.push(selectedMember.id);
    }    
    console.log(this.listSelectedMembers);
    //Adicionar um novo membro a lista listSelectedMember de object do Tipo selectedMember
  }

  dropSelectMember(memberId){
    var index = this.flag.indexOf(memberId);
    let position
    if (index > -1) {
      this.flag.splice(index, 1);

      //let position = this.listSelectedMembers.find(selectedMember => selectedMember.id === memberId).foo;
      for (var i = 0; i < this.listSelectedMembers.length; i++) {
        console.log("Object");
        console.log(this.listSelectedMembers[i]['id']);
        if (this.listSelectedMembers[i]['id'] === memberId) {
            position = i;
        }
      }
      this.listSelectedMembers.splice(position, 1);
      // this.listSelectedMembers.push(selectedMember);        
    }    
  }
}
