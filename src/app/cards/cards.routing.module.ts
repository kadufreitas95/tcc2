import { NgModule } from "@angular/core";
//import { CardDetalheComponent } from './card-detalhe/card-detalhe.component';
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CardsComponent } from './cards.component';
import { CardDetalheComponent } from "./card-detalhe/card-detalhe.component";
import { CreateCardComponent } from "./create-card/create-card.component";
import { EditCardComponent } from "./edit-card/edit-card.component";

const cardsRoutes : Routes = [
    {path: "cards", component: CardsComponent },
    {path: "cards/:id", component: CardDetalheComponent},
    {path: "create-card/:idList/:nameGO/:type", component: CreateCardComponent},
    {path: "edit-card/:id", component: EditCardComponent},
    {path: "", component: CardsComponent }
];

@NgModule({
    imports:[RouterModule.forChild(cardsRoutes)],
    exports:[RouterModule]
})
export class CardsRoutingModule{

}