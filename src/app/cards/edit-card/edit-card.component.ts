import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TrelloService } from '../../trello.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CardsService } from '../cards.service';

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.css','../create-card/create-card.component.css','../.././app.component.css']
})
export class EditCardComponent implements OnInit {

  formulario: FormGroup;
  backGroundCard:string = "#fff";
  display:string = 'none';
  response:any;
  carId:number;
  inscricao: Subscription;
  card:any;
  error:string;
  type:number;
  state:number;
  nameGO:string;
  date:any;
  arrayLabels:any = {};
  idList:any;
  listMembers:any;
  listSelectedMembers:any = new Array();
  flag:any = new Array;

  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private trelloService: TrelloService,private router: Router, private cardsService :CardsService) { }
  
  ngOnInit() {
    this.formulario = this.formBuilder.group({
      name:[null],
      desc:[null],
      due:[null],
      status:[null],
      type:[null]
    });

    this.inscricao = this.route.params.subscribe(
      (params:any) => {
          this.trelloService.getCardId(params['id']).subscribe(
            card => {
              this.trelloService.getLabelsBoard().subscribe(
                data => {
                  data.labels.forEach(element => {
                    // console.log(element);
                    this.arrayLabels[element.name] = element.id;
                    // this.arrayLabels.push(element.id);
                  });
                  this.trelloService.getMemberFromCard(params['id'])
                  .subscribe(
                    data =>{
                      data.forEach(member => {
                        let selectedMember:any = {
                          nome: member.fullName,
                          id:member.id
                        };
                        this.listSelectedMembers.push(selectedMember);  
                        this.flag.push(selectedMember.id);  
                      });
                    }
                  );
                  this.populaCampos(card);
                }
              );               
              this.trelloService.getNameGO(card.idList).subscribe(
                go => this.nameGO = go.name
              );
            }
          );        
      }
    );
    
    this.trelloService.getMemberFromBoard()
    .subscribe(
      data =>{
        console.log(data);
        this.listMembers = data
      }
    );
      
  }

  toggleColor(type){
    if (type == 0) {
      this.backGroundCard = "#FBBA42";
    }else{
      this.backGroundCard = "#56B9D0";
    }    
  }

  onSubmit(){
    this.trataParams(this.formulario.value);
  }

  trataParams(card){

    var params: string;
    params = "&name=" + card.name;
    params += "&desc=" + card.desc;
    if (card.due != '') {
      // params += "&due=" + form.value['bday'];  
      params += "&due=" + this.cardsService.setDateSubmit(card.due);  
    }
    params += "&idLabels=";
    //params += "&idList=";    
    console.log(card.status);
    switch (card.status) {
      case 1://Fazer
        params += this.arrayLabels.fazer;  
        break;
      case 2://Desenvolvendo
        params += this.arrayLabels.desenvolvendo;
        break;        
      case 3://Feito
        params += this.arrayLabels.feito;
        break;

      default:
        break;
    }
    
    if (card.type == 0) { //type = Arte
      params += ","+ this.arrayLabels.arte;
    }else{//type = Programacao
      params += ","+ this.arrayLabels.programacao;
    }

    for (var i = 0; i < this.listSelectedMembers.length; i++) {
      if (i > 0) {
        params += ","+ this.listSelectedMembers[i]['id'];
      }else{
        params += "&idMembers="+ this.listSelectedMembers[i]['id'];        
      }
      
    }

    this.trelloService.editCards(this.carId, params)
    .subscribe(
      data => {
        this.response = data;
      },
      error => this.error = error,
      ()=> {
        if(this.error == null){
          // this.router.navigate(['cards/', this.carId]);
          this.back();
        }
      }
      
    );
  }

  back(){
    // game-objects/sub-module-arte/:idList/:nameGO
    if(this.type == 0){
      var subModule = "sub-module-arte";
    }else if(this.type == 1){
      var subModule = "sub-module-programacao";
    }
    this.router.navigate(['/game-objects/' + subModule, this.idList]);
  }

  populaCampos(card){
    this.carId = card.id;
    this.idList = card.idList;
    card.labels.forEach(label => {
      if(label.id == this.arrayLabels.arte){ this.type = 0; this.backGroundCard = "#FBBA42"; this.type = 0;}
      if(label.id == this.arrayLabels.programacao){ this.type = 1; this.backGroundCard = "#56B9D0"; this.type = 1;} 
      if(label.id == this.arrayLabels.fazer){ this.state = 1;}
      if(label.id == this.arrayLabels.desenvolvendo){ this.state = 2;} 
      if(label.id == this.arrayLabels.feito){ this.state = 3;} 
    });    
    
    this.date = this.cardsService.setDate(card.due);
        
    this.formulario.patchValue({      
          name:card.name,
          desc:card.desc,
          due:this.date,
          status:this.state,
          type:this.type
        });
  }

  addSelectMember(member){  
    let selectedMember:any = {
      nome: member.fullName,
      id:member.id
    };
    let verifica = 0;

    this.flag.forEach(element => {
      if (element == selectedMember.id) {
        verifica ++;
      }      
    });

    if (verifica == 0) {
      this.listSelectedMembers.push(selectedMember);  
      this.flag.push(selectedMember.id);
    }    
    console.log(this.listSelectedMembers);
    //Adicionar um novo membro a lista listSelectedMember de object do Tipo selectedMember
  }

  dropSelectMember(memberId){
    var index = this.flag.indexOf(memberId);
    let position
    if (index > -1) {
      this.flag.splice(index, 1);

      //let position = this.listSelectedMembers.find(selectedMember => selectedMember.id === memberId).foo;
      for (var i = 0; i < this.listSelectedMembers.length; i++) {
        console.log("Object");
        console.log(this.listSelectedMembers[i]['id']);
        if (this.listSelectedMembers[i]['id'] === memberId) {
            position = i;
        }
      }
      this.listSelectedMembers.splice(position, 1);
      // this.listSelectedMembers.push(selectedMember);        
    }
    
  }

}
