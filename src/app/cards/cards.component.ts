import { TrelloService } from './../trello.service';

import { Component, OnInit ,EventEmitter} from '@angular/core';
import { AppComponent } from '../app.component';
import { ActivatedRoute, Router } from '@angular/router';
import { CardsService } from './cards.service';


@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css','.././app.component.css']
})
export class CardsComponent implements OnInit {

  private memberId: string;
  arrayLabels:any = {};
  // private id_board:string;

  constructor(private trelloService:TrelloService, private router: Router, private cardsService: CardsService) {
    this.memberId = localStorage.getItem('member');
    // this.id_board = "PaxTDig6";
    AppComponent.projectSet.subscribe(
      response => {
        if (response) {
          this.listaMyCards();  
          this.nameBoard();
        }
      }
    );
  }

  ngOnInit() {
    this.listaMyCards();  
    this.nameBoard();
  }

  arrayCards:any;
  arrayMyCards:any[];
  name_Board: string;
  cor:string;

  nameBoard(){
    this.trelloService.getNameBoard()
    .subscribe(
      data => this.name_Board = data,
      error => console.log(error),
      () => console.log(this.name_Board)
    );
  }

  setColorCard(card:any){
    card.labels.forEach(label => {
      if (label.name == "arte") {
        this.cor = '#FBBA42';
      }
      if (label.name == "programacao") {
        this.cor = '#56B9D0';
      }
    });
    return this.cor;
  }

  listaMyCards(){
    this.arrayMyCards= new Array();
    this.trelloService.getAllCards()
    .subscribe(
      data => {
        data.forEach(card => {
          card.idMembers.forEach(id => {
            if (id == this.memberId) {
              this.arrayMyCards.push(card);
            }
          });
        });    
      },
      error => console.log(error),
      () => console.log()
    );
    this.trelloService.getLabelsBoard().subscribe(
      data => {
        data.labels.forEach(element => {
          // console.log(element);
          this.arrayLabels[element.name] = element.id;
          // this.arrayLabels.push(element.id);
        });
      }
    );

    
    this.arrayCards = this.arrayMyCards;
    console.log(this.arrayCards);
    
  }

  redirect(cardId,idList,labels){
    // game-objects/sub-module-arte/:idList/:nameGO
    let type;
    labels.forEach(label => {
      if(label.id == this.arrayLabels.arte){ type = 0;}
      if(label.id == this.arrayLabels.programacao){ type = 1;}   
    });
    
    if(type == 0){
      var subModule = "sub-module-arte";
    }else if(type == 1){
      var subModule = "sub-module-programacao";
    }
    this.router.navigate(['/game-objects/' + subModule, idList]);
  }


}
