import { CardsService } from './cards.service';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CardsRoutingModule } from './cards.routing.module';
import { CardDetalheComponent } from './card-detalhe/card-detalhe.component';
import { CardsComponent } from './cards.component';
import { HttpModule } from '@angular/http';
import { CreateCardComponent } from './create-card/create-card.component';
import { EditCardComponent } from './edit-card/edit-card.component';
//import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    CardsRoutingModule,
    FormsModule,
    ReactiveFormsModule
    //RouterModule
  ],
  declarations: [
    CardsComponent,
    CreateCardComponent,    
    EditCardComponent
  ],
  exports:[
  ],
  providers:[CardsService]
})
export class CardsModule { }
