import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class CardsService {
    cardEmitter = new EventEmitter < any > ();
    mostrarMenuVerticalEmitter = new EventEmitter < boolean > ();
    setNameGO = new EventEmitter < string > ();
    myCardEmitter = new EventEmitter < any > ();
        
    constructor(private router: Router) { }

    setDate(date){
      var d = new Date(date);
      var currentDate = new Date();
      console.log("valor de d");
      console.log(d);
      
      if (d.getFullYear() < (currentDate.getFullYear() - 3)) {
        return ''  
      }else{
        if ((d.getDate() + 1) <= 10) {
          var day = '0' + d.getDate(); // Hours
        }else{
          var day = '' + d.getDate(); // Hours
        }
        
        if ((d.getMonth() + 1) <= 10) {
          var month = '0' + (d.getMonth() + 1);  
        }else{
          var month = '' + (d.getMonth() + 1);
        }
        var year = '' + d.getFullYear();
    
        return year +'-'+ month+'-' + day;
      }      
    }

    setDateSubmit(date){//Por termos -3h do UTC ao cadastrar o card ele ia para 1dia a menos 
      var d = new Date(date);
      var currentDate = new Date();
      console.log("valor de d");
      console.log(d);
      
      if (d.getFullYear() < (currentDate.getFullYear() - 3)) {
        return ''  
      }else{
        if ((d.getDate() + 1) < 10) {
          var day = '0' + (d.getDate() + 2); // Hours
        }else{
          var day = '' + (d.getDate() + 2); // Hours
        }
        
        if ((d.getMonth() + 1) < 10) {
          var month = '0' + (d.getMonth() + 1);  
        }else{
          var month = '' + (d.getMonth() + 1);
        }
        var year = '' + d.getFullYear();
    
        return year +'-'+ month+'-' + day;
      }      
    }

    getDate(date){
      var d = new Date(date);
      var currentDate = new Date();
      
      if (d.getFullYear() < (currentDate.getFullYear() - 3)) {
        return "Sem data definida"
      }else{
        if ((d.getDate() + 1) < 10) {
          var day = '0' + d.getDate(); // Hours
        }else{
          var day = '' + d.getDate(); // Hours
        }
        
        if ((d.getMonth() + 1) < 10) {
          var month = '0' + (d.getMonth() + 1);  
        }else{
          var month = '' + (d.getMonth() + 1);
        }
        var year = '' + d.getFullYear();

        return  day+'-'+ month+'-' + year;
      }
    }

    setCardDetalhe(cardId){
      this.cardEmitter.emit(cardId);
    }

}