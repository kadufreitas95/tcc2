import { CardsService } from './cards/cards.service';
import { TrelloService } from './trello.service';
import { Component, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  mostrarMenu: boolean = false;
  error:any;
  static projectSet = new EventEmitter < any > ();
  static projectCreate = new EventEmitter < any > ();
  labels = [
  {
    name: 'gcl',
    color:'sky'
  },{
    name: "fazer",
    color:'black'
  },{
    name: "feito",
    color:'green'
  },{
    name: "desenvolvendo",
    color:'orange'
  },{
    name: "programacao",
    color:'purple'
  },{
    name: "arte",
    color:'yellow'
  }];
  arrayMyBoards:any;
  notSetProject:boolean = true;
  nameProject:any;
  // mostrarNavVertical: boolean = false;
  nameGO:string = '';
  constructor(
    private router: Router,
    private trelloService:TrelloService,
    // private cardsService:CardsService
  ) {

    this.trelloService.mostrarMenuEmitter.subscribe(
      mostrar => this.mostrarMenu = mostrar 
    );

    if (localStorage.getItem('token') != null) {
      this.mostrarMenu = true;  
    }
    
  }

  ngOnInit(){
    this.listaMyBoards();
    this.setNameProject();
  }
  
  logout(){
    this.mostrarMenu = false;
    localStorage.removeItem('token');
    location.reload();
    this.router.navigate(['/login']);
  }

  onSubmit(form){
    var params:string;
    params = "&name=" + form.value['nome'];
    params += "&desc=gcl&defaultLabels=false&defaultLists=false&keepFromSource=none&prefs_permissionLevel=private&prefs_voting=disabled&prefs_comments=members&prefs_invitations=members&prefs_selfJoin=true&prefs_cardCovers=true&prefs_background=blue&prefs_cardAging=regular";
    // data => this.setLabels(data.id)
    this.trelloService.postProject(params)
    .subscribe(
      data => {
        this.labels.forEach(label => {
          this.trelloService.setLabels(data.id,label.name, label.color);  
        });                
        // AppComponent.projectSet.emit(data.id);
        //this.trelloService.mostrarMenuEmitter.emit(data.id);
        //console.log(data);
        form.resetForm();
        $('#modalProject').modal('hide');
        this.router.navigate(['/cards']);
        AppComponent.projectSet.emit(data.id);
        AppComponent.projectCreate.emit(true);
        this.listaMyBoards();
      },
      error => this.error = error
    );
    if (this.error != null) {
      alert("Error. Tivemos um problema com a criação do seu Projeto, tente novamente mais tarde.");
    }
  }

  listaMyBoards(){
    
    this.arrayMyBoards = new Array();
    
    this.trelloService.getMyBoards()
    .subscribe(
      data => {
        data.forEach(board => {
          //console.log(board.labelNames);
          // board.labelNames.forEach(label => {
          if (board.labelNames.sky == 'gcl'){
            this.arrayMyBoards.push(board);
          }
          // });
        });    
      },
      error => console.log(error),
      // () => console.log(this.arrayMyBoards)
    );
    
    // this.arrayCards = this.arrayMyCards;
    // console.log(this.arrayCards);
    
  }

  selectProject(id){
    AppComponent.projectSet.emit(id);
    AppComponent.projectCreate.emit(true);
    this.router.navigate(['/cards']);
    this.setNameProject();
  }

  setNameProject(){
    this.trelloService.getNameBoard().subscribe(
      board => {
        if (board.name == null) {
          this.nameProject = 'Projetos';
        }else{
          this.nameProject = board.name;
        }        
        console.log(board);
      }
    );
  }
  

}
