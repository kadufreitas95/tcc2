import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubModuleArteComponent } from './sub-module-arte.component';

describe('SubModuleArteComponent', () => {
  let component: SubModuleArteComponent;
  let fixture: ComponentFixture<SubModuleArteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubModuleArteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubModuleArteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
