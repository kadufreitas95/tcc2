import { GameObjectsComponent } from './../game-objects.component';
import { CardsService } from './../../cards/cards.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TrelloService } from '../../trello.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sub-module-arte',
  templateUrl: './sub-module-arte.component.html',
  styleUrls: ['./sub-module-arte.component.css','../game-objects.component.css','../.././app.component.css','../../cards/cards.component.css']
})
export class SubModuleArteComponent implements OnInit  {

  constructor(
    private route: ActivatedRoute,
    private trelloService:TrelloService,
    private cardsService:CardsService,
    private router: Router
  ) {}
  
  mostrarDetalhe:boolean = false;
  idList:string;
  nameLabel:string;
  nameGO:string;
  arrayCards:any;
  arrayCardsArte:any;
  inscricao: Subscription;
  response:any;

  show:boolean = false;

  // Variaveis de Detalhe
  cardId:any;

  ngOnInit() {  
    this.nameLabel = "arte";
    this.inscricao = this.route.params.subscribe(
      (params:any) => {
        this.idList = params['idList'];
        this.listaCardsModule(this.idList);
      }
    );
    // this.cardsService.mostrarMenu(this.nameGO);
  }

  getCardDetalhe(cardId){
    // this.mostrarDetalhe = true;
    this.show = true;
    this.cardsService.setCardDetalhe(cardId);
  }

  closeDetalhe(){
    this.show = false;
  }

  listaCardsModule(idList:string){
    this.arrayCardsArte = new Array();
    this.trelloService.getCardsModule(idList)
    .subscribe(
      data => {
        data.forEach(card => {
          card.labels.forEach(label => {
            // console.log(label);
            if (label.name == this.nameLabel) {
              this.arrayCardsArte.push(card);
            }
          });
        });    
      },
      error => console.log(error),
      () => console.log()
    );
    this.trelloService.getList(idList)
    .subscribe(
      list => this.nameGO = list.name
    );
    this.arrayCards = this.arrayCardsArte;
    //console.log(this.arrayCards);
  }
    
  feedback(response) {
    this.listaCardsModule(this.idList);
    this.show = false;
    console.log(response);
  }

  delete(){
    this.trelloService.deleteGO(this.idList)
    .subscribe(
      data => {
        this.response = data;
        this.router.navigate(['game-objects/']);
      },
      error => console.log(error)
    );
  }

}
