import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TrelloService } from '../../trello.service';
import { Subscription } from 'rxjs';
import { CardsService } from '../../cards/cards.service';

@Component({
  selector: 'app-sub-module-programacao',
  templateUrl: './sub-module-programacao.component.html',
  styleUrls: ['./sub-module-programacao.component.css','../game-objects.component.css','../.././app.component.css','../../cards/cards.component.css']
})
export class SubModuleProgramacaoComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private trelloService:TrelloService,
    private cardsService:CardsService
  ) { }

  idList:string;
  nameLabel:string;
  nameGO:string;
  arrayCards:any;
  arrayCardsArte:any;
  inscricao: Subscription;

  show:boolean = false;

  ngOnInit() {
    this.nameLabel = "programacao";
    this.inscricao = this.route.params.subscribe(
      (params:any) => {
        this.idList = params['idList'];
        this.listaCardsModule(this.idList);
      }
    );
  }

  listaCardsModule(idList:string){
    this.arrayCardsArte = new Array();
    this.trelloService.getCardsModule(idList)
    .subscribe(
      data => {
        data.forEach(card => {
          card.labels.forEach(label => {
            // console.log(label);
            if (label.name == this.nameLabel) {
              this.arrayCardsArte.push(card);
            }
          });
        });    
      },
      error => console.log(error),
      () => console.log()
    );
    this.trelloService.getList(idList)
    .subscribe(
      list => this.nameGO = list.name
    );
    this.arrayCards = this.arrayCardsArte;
    //console.log(this.arrayCards);
  }

  getCardDetalhe(cardId){
    // this.mostrarDetalhe = true;
    this.show = true;
    this.cardsService.setCardDetalhe(cardId);
  }

  closeDetalhe(){
    this.show = false;
  }

  feedback(response) {
    this.listaCardsModule(this.idList);
    this.show = false;
    console.log(response);
  }

}
