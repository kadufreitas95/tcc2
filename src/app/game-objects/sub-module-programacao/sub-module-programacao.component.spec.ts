import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubModuleProgramacaoComponent } from './sub-module-programacao.component';

describe('SubModuleProgramacaoComponent', () => {
  let component: SubModuleProgramacaoComponent;
  let fixture: ComponentFixture<SubModuleProgramacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubModuleProgramacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubModuleProgramacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
