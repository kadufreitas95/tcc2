import { NgModule } from "@angular/core";
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { GameObjectsComponent } from "./game-objects.component";
import { SubModuleArteComponent } from "./sub-module-arte/sub-module-arte.component";
import { SubModuleProgramacaoComponent } from "./sub-module-programacao/sub-module-programacao.component";

const gameObjectsRoutes : Routes = [
    {path: "game-objects", component: GameObjectsComponent },
    {path: "game-objects/sub-module-arte/:idList", component: SubModuleArteComponent},
    {path: "game-objects/sub-module-programacao/:idList", component: SubModuleProgramacaoComponent},
    // {path: "sub-module-arte/card/:idCard", component: CardArteComponent }
];

@NgModule({
    imports:[RouterModule.forChild(gameObjectsRoutes)],
    exports:[RouterModule]
})
export class GameObjectsRoutingModule{

}