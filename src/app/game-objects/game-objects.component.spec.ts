import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameObjectsComponent } from './game-objects.component';

describe('GameObjectsComponent', () => {
  let component: GameObjectsComponent;
  let fixture: ComponentFixture<GameObjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameObjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameObjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
