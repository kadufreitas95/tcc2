import { TrelloService } from './../trello.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

declare var $:any;

@Component({
  selector: 'app-game-objects',
  templateUrl: './game-objects.component.html',
  styleUrls: ['./game-objects.component.css','.././app.component.css',]
})
export class GameObjectsComponent implements OnInit {

  private gameObjects: any;
  private iconCards = "/assets/img/icon-cards.png";
  private iconPending = "/assets/img/icon-peding.png";

  constructor(private trelloService:TrelloService) {
  }

  anime:boolean = false;
  nameModule:any;
  board: string;
  idList:string;
  error:string;
  num:number = 0;


  ngOnInit() {
    this.listaGameObjects();
    this.nameBoard();
  }

  choiceSubModule(gameObject:any){
    this.anime = true;
    this.nameModule = gameObject.name;
    this.idList = gameObject.id;
  }

  closeSubModules(){
    this.anime = false;
  }

  listaGameObjects(){
    this.trelloService.getGameObjects()
    .subscribe(
      data => this.gameObjects = data,
      error => console.log(error),
      //() => console.log(this.gameObjects)
    );
    
  }

  nameBoard(){
    this.trelloService.getNameBoard()
    .subscribe(
      data => this.board = data,
      error => console.log(error),
      //() => console.log(this.board)
    );
  }

  cardsPending(object:any){
    let cont: number = 0;
    let nPending: number = 0;
    // console.log(object + " >> " + nPending);    
    object.forEach(card => {
      cont = 0;
      card.labels.forEach(label => {
        //console.log(label);
        if (label.name == "feito") {
          cont = 1;
        }
      });
      if (cont == 0) {
        nPending++;
      }
    });
    // console.log(nPending);
    return nPending;
  }

  progress(nTotal:number,nPending:number){
    
    let nFeitos =  nTotal - nPending;
    let percent = (100 * nFeitos)/nTotal;

    return percent.toFixed(0);
  }

  onSubmit(form){
    var params:string;
    params = "&name=" + form.value['nome'];

    this.trelloService.postGameObject(params)
    .subscribe(
      data => {
        form.resetForm();
        this.listaGameObjects();
        $('#exampleModalCenter').modal('hide');
      },
      error => this.error = error
    );
    if (this.error != null) {
      alert("Error. Tivemos um problema com a criação do seu card, tente novamente mais tarde.");
    }
  }

  generateColapse(){
    this.num = this.num + 1;
    console.log("#collapse" + this.num);
    
    // let next = num + 1;
    return "collapse" + (this.num);
  }

}
