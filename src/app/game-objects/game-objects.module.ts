import { CardDetalheComponent } from './../cards/card-detalhe/card-detalhe.component';
import { NgModule } from "@angular/core";
import { GameObjectsRoutingModule } from "./game-objects.routing.module";
import { CommonModule } from "@angular/common";
import { GameObjectsComponent } from "./game-objects.component";
import { SubModuleArteComponent } from "./sub-module-arte/sub-module-arte.component";
import { SubModuleProgramacaoComponent } from './sub-module-programacao/sub-module-programacao.component';
import { HttpModule } from "@angular/http";
import { NavVerticalComponent } from "../nav-vertical/nav-vertical.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        GameObjectsRoutingModule,
        FormsModule,
        ReactiveFormsModule
        //RouterModule
      ],
      declarations: [
        GameObjectsComponent,
        SubModuleArteComponent,
        SubModuleProgramacaoComponent,
        NavVerticalComponent,
        CardDetalheComponent
      ],
      exports:[NavVerticalComponent]
})
export class GameObjectsModule {}