import { TrelloService } from './../trello.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '.././app.component.css']
})
export class LoginComponent implements OnInit {
  private applicationkey:any;
  private applicationname:any;
  private token:any;

  constructor(private trelloService:TrelloService) {
    this.applicationkey = "136ea693d8ffd1de92fb99f769578c7b";
    this.applicationname = "GCL";
    this.token = "https://trello.com/1/authorize?key="+ this.applicationkey+"&name="+ this.applicationname+"&scope=read,write,account&expiration=never&response_type=token"
  }

  ngOnInit() {
  }

  onSubmit(form){
    this.trelloService.getToken(form.value['token']);
  }
}
