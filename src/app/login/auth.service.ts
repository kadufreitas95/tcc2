import { TrelloService } from './../trello.service';
import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  
  constructor(private trelloService: TrelloService,private router: Router) { }

  authToken(token:string){
    this.trelloService.getToken(token);
  }
}
